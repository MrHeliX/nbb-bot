import moment from "moment";
import needle from "needle";

export const sendWebhookMessage = async (message: string) => {
    const now = moment.utc();

    needle.post(process.env.DISCORD_WEBHOOK_URL, {
        username: "NDC Bot",
        content: `${now.format(`YYYY-MM-DD HH:mm:ss`)}: ${message}`
    }, { json: true });
};