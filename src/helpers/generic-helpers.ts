export const isDefined = <T>(item: T | undefined | null): item is T => !!item;

export const asyncForEach = async <T>(array: T[], callback: (elem: T, index: number, array: T[]) => any): Promise<void> => {
    for (let index = 0; index < array.length; index++) {
        await callback(array[index], index, array);
    }
};

export const parseUsernameToIRC = (name: string) => name.replaceAll(" ", "_");

export const modNames = {
    NF: "NoFail",
    HD: "Hidden",
    HR: "HardRock",
    DT: "DoubleTime"
};