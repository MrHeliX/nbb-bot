import moment from "moment";
import { Beatmap, ModBracket } from "../interfaces/db/beatmaps-interfaces";
import { IQualifiersLobby, MPLink, QualifiersScore } from "../interfaces/db/qualifiers-interfaces";
import { Registration } from "../interfaces/db/registrations-interfaces";
import { executeSqlQuery, executeSqlQueryFirstRow, executeSqlSingleInsert } from "../services/db";
import { getStateValue } from "../state/state";

const parseQualifiersLobbies = <T extends (IQualifiersLobby & MPLink)>(lobbies: T[]): T[] => {
    return lobbies.reduce((total, current) => {
        const existingLobby = total.find(t => t.uid === current.uid);
        if (!existingLobby) {
            const { mp_link, order, ...rest } = current;
            total.push({
                ...rest,
                mp_links: !!mp_link ? [{ mp_link, order }] : []
            });
        }
        else if (!!current.mp_link)
            existingLobby.mp_links.push({ mp_link: current.mp_link, order: current.order });
        return total;
    }, []);
};

export const dbGetBeatmapsInQualifiersPool = async (): Promise<Beatmap[]> => {
    return await executeSqlQuery<Beatmap>(`
        SELECT b.* FROM beatmaps b
        INNER JOIN mappools m ON b.mappool_uid = m.uid
        INNER JOIN tournament_rounds tr ON m.round_uid = tr.uid
        WHERE tr.acronym = "ql" AND tr.tourney_id = ?
    `, [getStateValue("tourney_id")]);
};

export const dbGetBeatmapFromQualifiersPool = async (map_id: number): Promise<Beatmap> => {
    return await executeSqlQueryFirstRow<Beatmap>(`
        SELECT b.* FROM beatmaps b
        INNER JOIN mappools m ON b.mappool_uid = m.uid
        INNER JOIN tournament_rounds tr ON m.round_uid = tr.uid
        WHERE tr.acronym = "ql" AND tr.tourney_id = ? AND b.map_id = ?;
    `, [getStateValue("tourney_id"), map_id]);
};

export const dbGetBeatmapFromQualifiersPoolByModBracket = async (mod_bracket: ModBracket, index: number): Promise<Beatmap> => {
    return await executeSqlQueryFirstRow<Beatmap>(`
        SELECT b.* FROM beatmaps b
        INNER JOIN mappools m ON b.mappool_uid = m.uid
        INNER JOIN tournament_rounds tr ON m.round_uid = tr.uid
        WHERE tr.acronym = "ql" AND tr.tourney_id = ? AND b.mod_bracket = ? AND b.mod_bracket_index = ?;
    `, [getStateValue("tourney_id"), mod_bracket, index]);
};

export const dbGetScheduledLobbiesForBot = async (): Promise<(IQualifiersLobby & { username: string })[]> => {
    const currentTime = moment.utc().set({ second: 0 }).format("YYYY-MM-DD HH:mm:ss");
    return await executeSqlQuery<IQualifiersLobby & { username: string }>(`
        SELECT l.*, u.user_id, u.username FROM qualifiers_lobbies l
        INNER JOIN player_registrations r ON l.uid = r.qualifiers_lobby_id
        INNER JOIN users u ON r.user_id = u.user_id
        WHERE l.tourney_id = ? AND l.is_active = FALSE AND l.referee = ? AND l.lobby_date = ?;
    `, [getStateValue("tourney_id"), +process.env.BOT_USER_ID, currentTime]);
};

export const dbGetScheduledLobbyForUser = async (user_id: number): Promise<IQualifiersLobby> => {
    const currentTime = moment.utc().set({ second: 0 });
    const maxAllowedTime = currentTime.clone().subtract(10, "minutes");

    return await executeSqlQueryFirstRow<IQualifiersLobby>(`
        SELECT l.*, r.user_id FROM qualifiers_lobbies l
        INNER JOIN player_registrations r ON l.uid = r.qualifiers_lobby_id
        WHERE l.tourney_id = ? AND r.user_id = ? AND l.is_active = FALSE AND l.referee = ? AND l.lobby_date >= ? AND l.lobby_date <= ?;
    `, [getStateValue("tourney_id"), user_id, +process.env.BOT_USER_ID, maxAllowedTime.format("YYYY-MM-DD HH:mm:ss"), currentTime.format("YYYY-MM-DD HH:mm:ss")]);
};

export const dbGetActiveQualifiersLobbiesForBot = async (): Promise<(IQualifiersLobby & { user_id: number, username: string, registration_uid: number })[]> => {
    const response = await executeSqlQuery<IQualifiersLobby & MPLink & { user_id: number, username: string, registration_uid: number }>(`
        SELECT l.*, mp.mp_link, mp.order, u.user_id, u.username, r.uid registration_uid FROM qualifiers_lobbies l
        LEFT JOIN mp_links mp ON l.uid = mp.qualifiers_lobby_id
        INNER JOIN player_registrations r ON l.uid = r.qualifiers_lobby_id
        INNER JOIN users u ON r.user_id = u.user_id
        WHERE l.tourney_id = ? AND l.is_active = TRUE AND l.referee = ?;
    `, [getStateValue("tourney_id"), +process.env.IRC_USERID]);

    return parseQualifiersLobbies(response);
};

export const dbGetQualifiersLobbyByUserId = async (user_id: number): Promise<IQualifiersLobby> => {
    const response = await executeSqlQuery<IQualifiersLobby & MPLink>(`
        SELECT l.*, mp.mp_link, mp.order FROM qualifiers_lobbies l
        LEFT JOIN mp_links mp ON l.uid = mp.qualifiers_lobby_id
        INNER JOIN player_registrations r ON l.uid = r.qualifiers_lobby_id
        WHERE l.tourney_id = ? AND r.user_id = ?;
    `, [getStateValue("tourney_id"), user_id]);

    return parseQualifiersLobbies(response)[0];
};

export const dbGetActiveQualifiersLobbyByUserId = async (user_id: number): Promise<IQualifiersLobby> => {
    const response = await executeSqlQuery<IQualifiersLobby & MPLink>(`
        SELECT l.*, mp.mp_link, mp.order, r.user_id FROM qualifiers_lobbies l
        LEFT JOIN mp_links mp ON l.uid = mp.qualifiers_lobby_id
        INNER JOIN player_registrations r ON l.uid = r.qualifiers_lobby_id
        WHERE r.user_id = ? AND l.tourney_id = ? AND l.is_active = TRUE;
    `, [user_id, getStateValue("tourney_id")]);

    return parseQualifiersLobbies(response)[0];
};

export const dbGetCurrentBeatmapInLobby = async (lobby_id: number): Promise<Beatmap> => {
    return await executeSqlQueryFirstRow<Beatmap>(`
        SELECT b.* FROM qualifiers_lobbies l
        INNER JOIN beatmaps b ON l.uid = ? AND l.current_map = b.uid;
    `, [lobby_id])
};

export const dbGetScoresForPlayerOnMap = async (player_uid: number, map_uid: number): Promise<QualifiersScore[]> => {
    return await executeSqlQuery<QualifiersScore>(`
        SELECT s.* FROM qualifiers_players_scores s
        WHERE s.user_uid = ? AND s.map_uid = ?;
    `, [player_uid, map_uid]);
};

export const dbGetSavedScoresQualifiersLobby = async (player_uid: number): Promise<QualifiersScore[]> => {
    return await executeSqlQuery<QualifiersScore>(`
        SELECT s.* FROM qualifiers_players_scores s
        WHERE s.user_uid = ?;
    `, [player_uid]);
};

export const dbUpdateCurrentMapQualifiersLobby = async (lobby_id: number, map_uid: number): Promise<void> => {
    await executeSqlQuery(`
        UPDATE qualifiers_lobbies
        SET current_map = ?
        WHERE uid = ?;
    `, [map_uid, lobby_id]);
};

export const dbMarkQualifiersLobbyAsStarted = async (lobby_id: number, mp_link: string, osu_lobby_id: number): Promise<void> => {
    await executeSqlQuery(`
        UPDATE qualifiers_lobbies
        SET osu_lobby_id = ?, is_active = TRUE
        WHERE uid = ?;
    `, [osu_lobby_id, lobby_id]);

    await executeSqlSingleInsert(`
        INSERT INTO mp_links (mp_link, qualifiers_lobby_id)
        VALUES (?, ?);
    `, [mp_link, lobby_id]);
};

export const dbMarkQualifiersLobbyAsClosed = async (lobby_id: number): Promise<void> => {
    await executeSqlQuery(`
        UPDATE qualifiers_lobbies
        SET is_active = FALSE, is_closed = TRUE
        WHERE uid = ?;
    `, [lobby_id]);
};

export const dbAddQualifiersScore = async (lobby_id: number, player_uid: number, score: QualifiersScore): Promise<void> => {
    await executeSqlSingleInsert(`
        INSERT INTO qualifiers_players_scores (lobby_uid, user_uid, map_uid, score, accuracy, count300, count100, count50, countmiss, maxcombo, attempt)
        VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);
    `, [lobby_id, player_uid, score.map_uid, score.score, score.accuracy, score.count300, score.count100, score.count50, score.countmiss, score.maxcombo, score.attempt]);
};

export const dbGetRegistrationByUserId = async (user_id: number): Promise<Registration> => {
    return await executeSqlQueryFirstRow<Registration>(`
        SELECT r.*, u.username FROM player_registrations r
        INNER JOIN users u ON r.user_id = u.user_id
        WHERE r.tourney_id = ? AND r.user_id = ? AND r.is_removed = FALSE;
    `, [getStateValue("tourney_id"), user_id]);
};

export const dbUpdateUsername = async (user_id: number, username: string): Promise<Registration> => {
    await executeSqlQuery(`
        UPDATE users
        SET username = ?
        WHERE user_id = ?;
    `, [username, user_id]);

    return await dbGetRegistrationByUserId(user_id);
};

export const dbGetHostByUserId = async (user_id: number): Promise<{ user_id: number }> => {
    return await executeSqlQueryFirstRow<{ user_id: number }>(`
        SELECT s.user_id FROM staff_users s
        WHERE s.tourney_id = ? AND s.user_id = ? AND (s.role_uid = 1 OR s.role_uid = 2 OR s.role_uid = 3);
    `, [getStateValue("tourney_id"), user_id]);
};

export const dbCreateQualifiersLobbyForUser = async (osu_lobby_id: number, mp_link: string, user_uid: number) => {
    const response = await executeSqlSingleInsert(`
        INSERT INTO qualifiers_lobbies (tourney_id, custom_id, osu_lobby_id, lobby_date, referee, is_active)
        VALUES (?, ?, ?, ?, ?, ?);
    `, [getStateValue("tourney_id"), 0, osu_lobby_id, moment.utc().format("YYYY-MM-DDTHH:mm:ss"), +process.env.IRC_USERID, true]);

    await executeSqlSingleInsert(`
        INSERT INTO mp_links (mp_link, qualifiers_lobby_id)
        VALUES (?, ?);
    `, [mp_link, response.insertId]);

    await executeSqlQuery(`
        UPDATE player_registrations
        SET qualifiers_lobby_id = ?
        WHERE uid = ?;
    `, [response.insertId, user_uid]);

    return response;
};
