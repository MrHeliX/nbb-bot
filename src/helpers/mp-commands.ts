import { Registration } from "../interfaces/db/registrations-interfaces";
import { getStateValue } from "../state/state";

const sendMessageToBanchoBot = (message: string): string[] => {
    const response = getStateValue("client")?.say("BanchoBot", message);
    return response;
};

export const sendMessageToLobby = (channel: string, message: string) => {
    const response = getStateValue("client")?.say(channel, message);
    return response;
};

export const createPrivateMultiplayerLobby = async (registration: Registration) => {
    return sendMessageToBanchoBot(`!mp makeprivate NDC: Qualifiers ${registration.username} (${registration.uid}-${registration.user_id})`);
};

export const closeMultiplayerLobby = async () => {
    sendMessageToBanchoBot(`!mp close`)
};

export const handleMultiplayerLobbyJoin = async (channel: string) => {
    sendMessageToLobby(channel, "!mp settings");
};