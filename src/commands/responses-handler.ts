import moment from "moment";
import { logAllChatMessages } from "../bot";
import { CommandResponse } from "../interfaces/generic-interfaces";
import { getStateValue, setUserAsAvailable } from "../state/state";
import { getUserFromStateById } from "../state/users";

const responsesQueue: { response: CommandResponse, time: number, user_id: number | undefined }[] = [];
let latestResponseTime: number = 0;

let queueTimer: NodeJS.Timer;

export const sendResponse = (response: CommandResponse[], user_id?: number): void => {
    responsesQueue.push(...response.map(r => ({ response: r, time: moment().valueOf(), user_id })));
    console.log(responsesQueue);

    if (!queueTimer)
        startQueueTimer();
};

const sendMessageToUser = (response: CommandResponse) => {
    const client = getStateValue("client");
    const { type, message, user } = response;
    if (!message.length) return;
    switch (type) {
        case "MESSAGE":
            client.say(user, message);
            logAllChatMessages(user, process.env.IRC_USERNAME, message);
            break;
        case "ACTION":
            client.action(user, message);
            break;
    };
};

const startQueueTimer = (): void => {
    queueTimer = setTimeout(async () => {
        const nextResponse = responsesQueue.shift();
        if (nextResponse) {
            sendMessageToUser(nextResponse.response);
            latestResponseTime = moment().valueOf();
            if (nextResponse.user_id) {
                const user = getUserFromStateById(nextResponse.user_id);
                setTimeout(() => {
                    if (user) {
                        setUserAsAvailable(user.username);
                        console.log(`Not busy anymore: ${user.username}`);
                    }
                }, 5000);
            }
        }
        if (responsesQueue.length)
            startQueueTimer();
        else
            queueTimer = undefined;
    }, Math.min(1000 - (moment().valueOf() - latestResponseTime), 0));
};

// Make sure nothing gets stuck
setInterval(() => {
    if (!queueTimer && responsesQueue.length && moment().valueOf() - responsesQueue[0].time > 1000)
        startQueueTimer();
}, 3000);