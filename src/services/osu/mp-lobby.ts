import { MPLinkMatch } from "../../interfaces/osu/mp-link";
import { osuApiV2Request } from "./api";

export const osuGetMatchData = async (match_id: number, token: string): Promise<MPLinkMatch> => {
    return await osuApiV2Request<MPLinkMatch>(`matches/${match_id}`, token);
};