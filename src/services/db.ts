import { createPool, Pool, ResultSetHeader } from "mysql2/promise";
import fs from "fs";
import { createTunnel, ForwardOptions, SshOptions, TunnelOptions } from "tunnel-ssh";

let dbConnection: Pool;

export const initializeDbConnection = async () => {
    const tunnelOptions: TunnelOptions = {
        autoClose: false
    };

    const sshOptions: SshOptions = {
        host: process.env.SSH_HOST,
        port: 22,
        username: process.env.SSH_USER,
        privateKey: fs.readFileSync(process.env.SSH_KEY_PATH)
    };

    const forwardOptions: ForwardOptions = {
        dstAddr: process.env.SQL_DB_HOST,
        dstPort: +process.env.SQL_DB_PORT,
    };

    const [server] = await createTunnel(tunnelOptions, {}, sshOptions, forwardOptions);
    const port = (server.address() as any).port;
    console.log(`Tunnel created on port ${port}`);

    dbConnection = await createPool({
        host: process.env.SQL_DB_HOST,
        port,
        user: process.env.SQL_DB_USER,
        password: process.env.SQL_DB_PASSWORD,
        database: process.env.SQL_DB_DATABASE
    });

    setInterval(async () => {
        const connection = await dbConnection.getConnection();
        await connection.ping();
        await connection.release();
    }, 60000);
};

export const executeSqlQuery = async <T>(query: string, params?: any[]): Promise<T[]> => {
    try {
        const connection = await dbConnection.getConnection();
        const [rows] = await connection.query(query, params);
        await connection.release();
        return rows as T[] || [];
    } catch (error) {
        console.error(error);
    }
};

export const executeSqlQueryFirstRow = async <T>(query: string, params?: any[]): Promise<T> => {
    return (await executeSqlQuery<T>(query, params))[0];
};

export const executeSqlInsert = async (query: string, params?: any[]): Promise<ResultSetHeader[]> => {
    try {
        const connection = await dbConnection.getConnection();
        const res = await connection.query(query, params) as ResultSetHeader[];
        await connection.release();
        return res;
    } catch (error) {
        console.error(error);
    }
};

export const executeSqlSingleInsert = async (query: string, params?: any[]): Promise<ResultSetHeader> => {
    return await (await executeSqlInsert(query, params)).filter(r => !!r)[0];
};
