import { dbGetBeatmapsInQualifiersPool } from "../helpers/db-queries";
import { Beatmap } from "../interfaces/db/beatmaps-interfaces";
import { getStateValue, setStateValue } from "./state";

export const getMappoolFromDb = async (): Promise<void> => {
    const mappool = await dbGetBeatmapsInQualifiersPool();
    setStateValue("mappool", mappool);
};

export const getMappoolFromState = (): Beatmap[] => {
    return getStateValue("mappool");
};