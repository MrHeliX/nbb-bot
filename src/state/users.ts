import { User } from "../interfaces/generic-interfaces";
import { getStateValue, setStateValue } from "./state";

export const addUserToState = (username: string, user_id: number, registration_id?: number): void => {
    const users = getStateValue("users");
    if (!users.some(u => u.user_id === user_id))
        setStateValue("users", [...users, { user_id, username, registration_id: registration_id ?? null }]);
};

export const addRegistrationIdToUserInState = (user_id: number, registration_id: number): void => {
    const user = getStateValue("users").find(u => u.user_id === user_id);
    if (user)
        user.registration_id = registration_id;
};

export const getUserFromStateByName = (username: string): User | undefined => {
    return getStateValue("users").find(u => u.username === username.replaceAll(" ", "_"));
};

export const getUserFromStateById = (user_id: number): User | undefined => {
    return getStateValue("users").find(u => u.user_id === user_id);
};
