import { QualifiersLobby } from "../qualifiers/QualifiersLobby";
import { getStateValue, setStateValue } from "./state";

export const addQualifiersLobby = (lobby: QualifiersLobby): void => {
    const lobbies = getStateValue("lobbies");
    if (!lobbies.some(l => l.osu_lobby_id === lobby.osu_lobby_id))
        setStateValue("lobbies", [...lobbies, lobby]);
};

export const getLobbyFromStateByChannel = (channel: string): QualifiersLobby | undefined => {
    return getStateValue("lobbies").find(l => l.channel === channel);
};

export const getLobbyFromStateByPlayer = (user_id: number): QualifiersLobby | undefined => {
    return getStateValue("lobbies").find(l => l.player.user_id === user_id);
};