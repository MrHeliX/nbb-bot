export interface Registration {
    uid: number,
    user_id: number,
    tourney_id: number,
    username: string,
    acronym: string | null,
    registration_date: string,
    global_rank: number,
    country_rank: number,
    avatar_url: string,
    is_removed: boolean,
    qualifiers_lobby: number | null,
    avatar: Buffer | null,
    status: number | null,
    seed: number | null,
    country: string
};