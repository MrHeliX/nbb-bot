export interface IQualifiersLobby {
    uid: number,
    tourney_id: number,
    custom_id: number,
    osu_lobby_id: number,
    lobby_date: string | null,
    referee: number | null,
    current_map: number | null,
    is_active: boolean,
    is_closed: boolean,
    mp_links: MPLink[]
};

export interface QualifiersScore {
    uid?: number,
    lobby_id: number,
    player_id: number,
    map_uid: number,
    score: number,
    accuracy: number,
    count300: number,
    count100: number,
    count50: number,
    countmiss: number,
    maxcombo: number,
    attempt: number
};

export interface MPLink {
    mp_link: string,
    match_uid?: number | null,
    qualifiers_lobby_id?: number | null,
    order: number
};