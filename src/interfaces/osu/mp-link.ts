export type MPLinkMatchDetailType = "match-created" | "player-joined" | "player-left" | "host-changed" | "match-disbanded" | "other";

export interface MPLinkMatch {
    match: {
        id: number,
        start_time: string,
        end_time: string,
        name: string
    },
    events: {
        id: number,
        detail: {
            type: MPLinkMatchDetailType,
            text?: string
        },
        timestamp: string,
        user_id: number,
        game?: {
            id: number,
            start_time: string,
            end_time: string,
            mode: string,
            mode_int: number,
            scoring_type: string,
            team_type: string,
            mods: string[],
            beatmap: {
                difficulty_rating: number,
                id: number,
                mode: string,
                status: string,
                total_length: number,
                version: string,
                beatmapset: unknown
            },
            scores: {
                id: unknown,
                user_id: number,
                accuracy: number,
                mods: string[],
                score: number,
                max_combo: number,
                perfect: number,
                statistics: {
                    count_50: number,
                    count_100: number,
                    count_300: number,
                    count_geki: number,
                    count_katu: number,
                    count_miss: number
                },
                rank: unknown,
                created_at: unknown,
                best_id: number,
                pp: number,
                match: {
                    slot: number,
                    team: string,
                    pass: number
                }
            }[]
        }
    }[],
    users: {
        avatar_url: string,
        country_code: string,
        default_group: string,
        id: number,
        is_active: boolean,
        is_bot: boolean,
        is_deleted: boolean,
        is_online: boolean,
        is_supported: boolean,
        last_visit: string,
        pm_friends_only: boolean,
        profile_colour: unknown,
        username: string,
        country: unknown
    }[],
    first_event_id: number,
    latest_event_id: number,
    current_game_id: number
};