osu! IRC bot for the qualifiers stage of the [Dutch Baby Bracket]

## How to run
- Make sure you have [NodeJS] and npm installed (npm comes with Node)
- Clone this repository to your pc
- Run `npm install` to install the dependencies
- Add a `.env` file in the root folder and fill it with the required environment variables (template below)
- Start the bot with `npm start`

## Environment variables template
```
IRC_SERVER=
IRC_PORT=
IRC_USERNAME=
IRC_PASSWORD=

BOT_PREFIX=!

OSU_API_KEY=
```

## Licensing
You are allowed to with this whatever you want, as long as you properly credit me ([Mr HeliX])

[Dutch Baby Bracket]: <https://baby.huismetbenen.nl>
[NodeJS]: <https://nodejs.org>
[Mr HeliX]: <https://osu.ppy.sh/u/2330619>